package com;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Node implements Cloneable {

	private int X,Y;
	private String Name;
    private LinkedList<Node> shortestPath = new LinkedList<>();
    private int distance = Integer.MAX_VALUE;
    private double time = Double.MAX_VALUE;
    private Map<Node, Integer> AdjacentNodes = new HashMap<>();
    private Map<Node, Double> AdjacentNodesTime = new HashMap<>();
    public Object clone() throws CloneNotSupportedException {
    	return super.clone();
    }
    public Node() {}
    public Node(int X,int Y) {
		this.X = X;
		this.Y = Y;
    }
	public void setX(int X) {
		this.X = X;
	}
	public void setName(String Name) {
		this.Name = Name;
	}
	public void setY(int Y) {
		this.Y = Y;
	}
	public int getX() {
		return this.X;
	}
	public String getName() {
		return this.Name;
	}
	public int getY() {
		return this.Y;
	}
    public void addDestination(Node destination, int distance) {
        AdjacentNodes.put(destination, distance);
    }
    public void addDestinationTime(Node destination, double time) {
        AdjacentNodesTime.put(destination, time);
    }
    public Map<Node, Integer> getAdjacentNodes() {
        return AdjacentNodes;
    }
    public void setAdjacentNodes(Map<Node, Integer> AdjacentNodes) {
        this.AdjacentNodes = AdjacentNodes;
    }
    public Map<Node, Double> getAdjacentNodesTime() {
        return AdjacentNodesTime;
    }
    public void setAdjacentNodesTime(Map<Node, Double> AdjacentNodesTime) {
        this.AdjacentNodesTime = AdjacentNodesTime;
    }
    public Integer getDistance() {
        return distance;
    }
    public void setDistance(int distance) {
        this.distance = distance;
    }
    public Double getTime() {
        return time;
    }
    public void setTime(double time) {
        this.time = time;
    }
    public List<Node> getShortestPath() {
        return shortestPath;
    }
    public void setShortestPath(LinkedList<Node> shortestPath) {
        this.shortestPath = shortestPath;
    }
}