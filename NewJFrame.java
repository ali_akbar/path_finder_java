package com;
import java.awt.image.*;

import  javax.swing.*;

import java.awt.event.*;
import java.awt.*;

import java.awt.geom.*;
public class NewJFrame extends  JFrame {

    
    public NewJFrame() {
        initComponents();
    }

                             
    private void initComponents() {
    	
        buttonGroup1 = new  ButtonGroup();
        buttonGroup2 = new  ButtonGroup();
        jPanel1 = new  JPanel();
        jLabel1 = new  JLabel();
        jRadioButton2 = new  JRadioButton();
        jRadioButton5 = new  JRadioButton();
        jRadioButton6 = new  JRadioButton();
        jLabel2 = new  JLabel();
        jRadioButton1 = new  JRadioButton();
        jRadioButton3 = new  JRadioButton();
        jLabel3 = new  JLabel();
        jComboBox1 = new  JComboBox<>();
        jLabel4 = new  JLabel();
        jComboBox2 = new  JComboBox<>();
        jLabel5 = new  JLabel();
        jComboBox3 = new  JComboBox<>();
        jComboBox4 = new  JComboBox<>();
        jButton2 = new  JButton();
        jComboBox5 = new  JComboBox<>();
        jComboBox6 = new  JComboBox<>();
        jComboBox7 = new  JComboBox<>();
        jLabel6 = new  JLabel();
        String s[] = {"Dharamshala",
        		"Hyderabad",	"Kolkata",	"Patna",	"Chandigarh",	"Delhi",	"Ahmadabad",	"Surat",
        		"Vadodara",	"Jammu",	"Srinagar",	"Bangalore",	"Kochi",	"Bhopal",  "Indore",	
        		"Mumbai",	"Lonavale",	"Nagpur",	"Pune",	"Darjeeling",	"Bhubaneswar",	"Amritsar",
        		"Jaipur",	"Jodhpur",	"Chennai",	"Agra",	"Allahabad",	"Noida",	"Raipur",	"Ranchi"	
        };
        setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE);
        setResizable(true);

        jLabel1.setText("MEANS OF TRANSPORT");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setText("RAIL");

        buttonGroup1.add(jRadioButton5);
        jRadioButton5.setText("FLIGHT");
        jRadioButton5.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jRadioButton5ActionPerformed(evt);
            }
        });
        jRadioButton2.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });
        jRadioButton3.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });
 

        buttonGroup1.add(jRadioButton6);
        jRadioButton6.setText("ROAD");
        jRadioButton6.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jRadioButton6ActionPerformed(evt);
            }
        });

        jLabel2.setText("SHORTEST PATH PARAMETER");

        buttonGroup2.add(jRadioButton1);
        jRadioButton1.setText("DISTANCE");
        jRadioButton1.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup2.add(jRadioButton3);
        jRadioButton3.setText("TIME");

        jLabel3.setText("SOURCE");

        jComboBox1.setMaximumRowCount(100);
        jComboBox1.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox1.setSelectedIndex(-1);
        jComboBox1.setToolTipText("");

        jLabel4.setText("DESTINATION");

        jComboBox2.setMaximumRowCount(100);
        jComboBox2.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox2.setSelectedIndex(-1);

        jLabel5.setText("VIA");

        jComboBox3.setMaximumRowCount(100);
        jComboBox3.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox3.setSelectedIndex(-1);

        jComboBox4.setMaximumRowCount(100);
        jComboBox4.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox4.setSelectedIndex(-1);
        jButton2.setText("SUBMIT");
        jButton2.addActionListener(new   ActionListener() {
            public void actionPerformed(  ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jComboBox5.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox5.setSelectedIndex(-1);

        jComboBox6.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox6.setSelectedIndex(-1);

        jComboBox7.setModel(new  DefaultComboBoxModel<>(s));
        jComboBox7.setSelectedIndex(-1);

        jLabel6.setFont(new java.awt.Font("Vani", 0, 24)); // NOI18N
        jLabel6.setText("PEREGRINATION");

         GroupLayout jPanel1Layout = new  GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addGap(117, 117, 117)
                        .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jRadioButton2)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton5)
                                .addGap(18, 18, 18)
                                .addComponent(jRadioButton6))
                            .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jComboBox7, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox6,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox5,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox4,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox3,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox2,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jComboBox1,  GroupLayout.Alignment.LEADING, 0,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jRadioButton1)
                                    .addGap(18, 18, 18)
                                    .addComponent(jRadioButton3)))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(248, 248, 248)
                        .addComponent(jButton2,  GroupLayout.PREFERRED_SIZE, 89,  GroupLayout.PREFERRED_SIZE))
                    .addGroup( GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6)
                        .addGap(152, 152, 152)))
                .addContainerGap(108, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton2)
                    .addComponent(jRadioButton5)
                    .addComponent(jRadioButton6)
                    .addComponent(jLabel1,  GroupLayout.PREFERRED_SIZE, 23,  GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jRadioButton1)
                    .addComponent(jRadioButton3))
                .addGap(48, 48, 48)
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4,  GroupLayout.PREFERRED_SIZE, 20,  GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup( GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox3,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5,  GroupLayout.DEFAULT_SIZE, 79, Short.MAX_VALUE))
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox4,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jComboBox5,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(jComboBox6,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jComboBox7,  GroupLayout.PREFERRED_SIZE,  GroupLayout.DEFAULT_SIZE,  GroupLayout.PREFERRED_SIZE)
                .addPreferredGap( LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jButton2))
        );

         GroupLayout layout = new  GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup( GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1,  GroupLayout.DEFAULT_SIZE,  GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }                       

    private void jRadioButton5ActionPerformed(  ActionEvent evt) {                                              
        mode = 0;
    	//System.out.println("F");
    }     
    private void jRadioButton6ActionPerformed(  ActionEvent evt) {                                              
    	mode= 4;
    	//System.out.println("Road");
    }     
    private void jButton2ActionPerformed(  ActionEvent evt) {                                              
        String s = Integer.toString(mode + parameter);
      //  g.setColor(Color.RED);
        if(jComboBox1.getSelectedIndex() != -1) {
        	s += " " + jComboBox1.getSelectedItem().toString();
        	if(jComboBox3.getSelectedIndex() != -1) {
        		s += " " + jComboBox3.getSelectedItem().toString();
        	}
        	else if(jComboBox4.getSelectedIndex() != -1) {
        		s += " " + jComboBox4.getSelectedItem().toString();
        	}
        	else if(jComboBox5.getSelectedIndex() != -1) {
        		s += " " + jComboBox5.getSelectedItem().toString();
        	}
        	else if(jComboBox6.getSelectedIndex() != -1) {
        		s += " " + jComboBox6.getSelectedItem().toString();
        	}
        	else if(jComboBox7.getSelectedIndex() != -1) {
        		s += " " + jComboBox7.getSelectedItem().toString();
        	}
        	if(jComboBox2.getSelectedIndex() != -1) {
        		s += " " + jComboBox2.getSelectedItem().toString();
        		new Main(s);
        	}
//        	else
//        		repaint();
        		
        }
//        else
//        	repaint();
        	
    }                                             
//    public void paint(Graphics g)
//    {
//    	boolean flag = jComboBox2.getSelectedIndex() != -1 ? true : false;
//    	if(flag)
//    		g.drawString("Please enter destination",150,150);
//    	else
//    		g.drawString("Please enter source",150,150);
//    }
    private void jRadioButton1ActionPerformed(  ActionEvent evt) {                                              
        parameter = 1;
        //System.out.println("D");
    }                                             
    private void jRadioButton2ActionPerformed(  ActionEvent evt) {                                              
        mode = 2;
        //System.out.println("Rail");
    }                                             
    private void jRadioButton3ActionPerformed(  ActionEvent evt) {                                              
        parameter = 0;
        //System.out.println("T");
    }                                             

   
    public static void main(String args[]) {
        
        try {
            for ( UIManager.LookAndFeelInfo info :  UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                     UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch ( UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewJFrame().setVisible(true);
            }
        });
    }

    private int parameter;
    private int mode;
    private  ButtonGroup buttonGroup1;
    private  ButtonGroup buttonGroup2;
    private  JButton jButton2;
    private  JComboBox<String> jComboBox1; //src
    private  JComboBox<String> jComboBox2; //dest
    private  JComboBox<String> jComboBox3; //via 1 
    private  JComboBox<String> jComboBox4; //via 2
    private  JComboBox<String> jComboBox5; //via 3
    private  JComboBox<String> jComboBox6; //via 4
    private  JComboBox<String> jComboBox7; //via 5
    private  JLabel jLabel1;
    private  JLabel jLabel2;
    private  JLabel jLabel3;
    private  JLabel jLabel4;
    private  JLabel jLabel5;
    private  JLabel jLabel6;
    private  JPanel jPanel1;
    private  JRadioButton jRadioButton1;
    private  JRadioButton jRadioButton2;
    private  JRadioButton jRadioButton3;
    private  JRadioButton jRadioButton5;
    private  JRadioButton jRadioButton6;
    public String s[] = new String[7];                 
}