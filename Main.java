package com;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.awt.BorderLayout;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.*;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.*;
import java.awt.geom.*;
import java.awt.event.*;
/**
 *
 * @author SONY
 */
public class Main {
	String s;
	private JFrame frame = new JFrame();
	private JLayeredPane lpane = new JLayeredPane();
	private JPanel panel1 = new MyPanel("D:\\code\\csaa\\New folder\\final2.png");
	private JPanel panel2 = new JPanel();
	public Main(String s)
	{
		this.s = s;
		frame.setPreferredSize(new Dimension(827, 976));
		frame.setLayout(new BorderLayout());
		frame.add(lpane, BorderLayout.CENTER);
		lpane.setBounds(0, 0, 827, 976);
		panel1.setBounds(0, 0, 827, 976);
		panel1.setOpaque(true);
//      panel2.add(linedraw1);
		panel2.setLayout(null);
		panel2.setBounds(0, 0, 700, 900);
		panel2.setOpaque(false);
		lpane.add(panel1, Integer.valueOf(0), 0);
		lpane.add(panel2, Integer.valueOf(1), 0);
//		JLabel jb = new JLabel("Button");
//		jb.setBounds(410, 550, 40, 40);
//		panel2.add(jb);
		frame.pack();
		frame.setVisible(true);
	}
	// This is your custom panel
	class MyPanel extends JPanel {
		private static final long serialVersionUID = -4559408638276405147L;
		private String imageFile;
		public MyPanel(String imageFile) {
			this.imageFile = imageFile;
		}
		protected void paintComponent(Graphics g) {
			// Add your image here
			Image img = new ImageIcon(imageFile).getImage();
			g.drawImage(img, 0, 0, this);
			Graphics2D g2=(Graphics2D) g;
			//String example = "5 Delhi Noida";
			String sample[] = s.split(" ",0);
			List<Node> path = DatabaseTest.SetPath(sample);
			// Add your lines here
			//g2.setColor(Color.red);
//			Map<Node,Integer> mp = path.get(5).getAdjacentNodes();
//			System.out.println(mp.get(path.get(6)));
//			for(Entry<Node, Integer> e : mp.entrySet())
//			{
//				System.out.println(e.getKey().getName() + e.getValue());
//			}
			g2.setStroke(new BasicStroke(3));
			if(Integer.parseInt(sample[0]) % 2 == 1)
			{
				g2.setColor(Color.gray);
			}
			for(int i=0;i<path.size()-1;i++)
			{
//				try {
//					Thread.sleep(5000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				if(Integer.parseInt(sample[0]) % 2 == 0)
				{
					double weight = path.get(i).getAdjacentNodesTime().get(path.get(i+1));
					if(weight<=3.33)
						g2.setColor(Color.red);
					else if(weight<=6.66)
						g2.setColor(Color.orange);
					else
						g2.setColor(Color.blue);
				}
				g2.drawLine(path.get(i).getX(), path.get(i).getY(), path.get(i+1).getX(), path.get(i+1).getY());				
			}
			g2.setColor(Color.black);
			for(Node node : path)
			{
				JLabel jb = new JLabel(node.getName());
				jb.setBounds(node.getX()+10,node.getY()-7, 80, 20);
				panel2.add(jb);
				g2.fillRect(node.getX()-2,node.getY()-2, 5, 5);				
			}
			boolean isRouteAvailable = true;
			if(Integer.parseInt(sample[0]) % 2 == 0)
			{
				if(path.get(path.size()-1).getDistance()==Integer.MAX_VALUE)
					isRouteAvailable = false;
				if(isRouteAvailable)
				{
					//System.out.println("total distance : " + path.get(path.size()-1).getDistance() + " km");
					Double time = 0.0;
					for(int i=0;i<path.size()-1;i++)
					{
						time = time + path.get(i).getAdjacentNodesTime().get(path.get(i+1));
					}
					//System.out.println("total time : " + time + " hr");
					g2.setColor(Color.WHITE);
					g2.fillRect(80,80,190,60);
					g2.setColor(Color.BLACK);
					g2.drawRect(80,80,190,60);
		            g2.drawString("Total Kilometre:"+ path.get(path.size()-1).getDistance() + " km",100,100);
		            g2.drawString("Total Time Required: "+ Math.round(time*100D)/100D + " hrs",100,120);
				}
				else
				{
					System.out.println("no route available");
				}
			}
			else
			{
				if(path.get(path.size()-1).getTime()==Double.MAX_VALUE)
					isRouteAvailable = false;
				if(isRouteAvailable)
				{
					//System.out.println("total time : " + path.get(path.size()-1).getTime() + " hr");
					int dist = 0;
					for(int i=0;i<path.size()-1;i++)
					{
						dist = dist + path.get(i).getAdjacentNodes().get(path.get(i+1));
					}
					//System.out.println("total distance : " + dist + " km");	
					g2.setColor(Color.WHITE);
					g2.fillRect(80,80,190,60);
					g2.setColor(Color.BLACK);
					g2.drawRect(80,80,190,60);
		            g2.drawString("Total Kilometre:"+ dist + " km",100,100);
		            g2.drawString("Total Time Required:"+ Math.round(path.get(path.size()-1).getTime()*100D)/100D + " hrs",100,120);
				}
				else
				{
					System.out.println("no route available");
				}				
			}
			// g.setColor(Color.black);
			// g.drawLine(0, g.getClipBounds().height, g.getClipBounds().width, 0);
		}
	}
	/**
	 * 
	 * @param args the command line arguments
	 * 
	 */
//	public static void main(String[] args) {
//		new Main();
//	}
}
