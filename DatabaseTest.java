package com;
import java.io.*;
import java.sql.*;
//import oracle.jdbc.driver.*;
//import java.io.*;
//import com.*;
import java.util.*;
import java.util.Map.Entry;

public class DatabaseTest {
/*    public static void main(String args[])throws Exception{
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\user\\new1.txt"));
		String temp = br.readLine();
		String query = new String();
		while(temp!=null)
		{
			query = query + temp + "\n";
			temp = br.readLine();
		}
		String arr[] = query.split("\n\n",0);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection connection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "MAIN", "mtfougu");
  	    Statement stmt = connection.createStatement();
		
		for(String s : arr)
		{
			if(s!=null)
			{
				System.out.println(s);
				stmt.executeUpdate(s);				
			}
		}
		stmt.close();
		connection.close();
    }
*/	public static void GetAdjacencyList(Map<String,Integer> namemap, Map<Integer,String> revnamemap, Map<Integer,Node> nodemap, Map<Node,Integer> revnodemap,int flag) throws Exception {
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection c = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","MAIN","mtfougu");
		Statement st = c.createStatement();
		String var ;
		if(flag==0 || flag==1)
			var = "EDGE";
		else if(flag==2 || flag==3)
			var = "TRAIN";
		else 
			var = "FLIGHT";
		ResultSet rs = st.executeQuery("select * from VERTEX");
		while(rs.next())
		{
			Node tempnode = new Node();
			namemap.put(rs.getString(2),rs.getInt(1));
			revnamemap.put(rs.getInt(1),rs.getString(2));
			tempnode.setX(Integer.parseInt(rs.getString(3)));
			tempnode.setY(Integer.parseInt(rs.getString(4)));
			tempnode.setName(rs.getString(2));
			nodemap.put(rs.getInt(1),tempnode);
			revnodemap.put(tempnode,rs.getInt(1));
		}
		ResultSet rt = st.executeQuery("select * from " + var);
		int to_id = 0,from_id = 0,weight = 0;
		double time = 0;
		while(rt.next())
		{
			to_id = rt.getInt(2);
			from_id = rt.getInt(3);
			weight = rt.getInt(4);
			time = rt.getDouble(6);
			nodemap.get(to_id).addDestination(nodemap.get(from_id),weight);
			nodemap.get(from_id).addDestination(nodemap.get(to_id),weight);
			nodemap.get(to_id).addDestinationTime(nodemap.get(from_id),time);
			nodemap.get(from_id).addDestinationTime(nodemap.get(to_id),time);
		}
		st.close();
		c.close();
	}
	public static List<Node> SetPath(String sample[]) {
//public static void main(String args[]) throws Exception {
		List<Node> finalpath = new LinkedList<>();
		try {
//			for(int i=1;i<sample.length-1;i++)
//			{
//				System.out.println(i + sample[i] + (i+1));
//			}
			Map<String,Integer> namemap = new HashMap<>();
			Map<Integer,String> revnamemap = new HashMap<>();
			Map<Integer,Node> nodemap = new HashMap<>();
			Map<Node,Integer> revnodemap = new HashMap<>();
			GetAdjacencyList(namemap ,revnamemap ,nodemap ,revnodemap,Integer.parseInt(sample[0]));
			int finaldistance = 0;
			double finaltime = 0;
			for(int i=1;i<sample.length-1;i++)
			{
				if(Integer.parseInt(sample[0]) % 2 == 0)
				{
					Dijkstra.calculateShortestPathFromSource(nodemap,nodemap.get(namemap.get(sample[i])));					
				}
				else
				{
					Dijkstra.calculateShortestTimeFromSource(nodemap,nodemap.get(namemap.get(sample[i])));
				}
				List<Node> templist = nodemap.get(namemap.get(sample[i+1])).getShortestPath();
//				for(Node n : templist)
//				{
//					System.out.println(revnamemap.get(revnodemap.get(n))+" "+n.getDistance());
//				}
				if(i!=1 && templist!=null)
				{
					templist.remove(0);
				}
				for(Node node : templist)
				{
					//System.out.println(revnamemap.get(revnodemap.get(node))+" "+node.getDistance());
					if(Integer.parseInt(sample[0]) % 2 == 0)
					{
						node.setDistance(finaldistance + node.getDistance());
					}
					else
					{
						node.setTime(finaltime + node.getTime());
					}
					finalpath.add((Node)node.clone());
				}
				if(Integer.parseInt(sample[0]) % 2 == 0) 
				{
					nodemap.get(namemap.get(sample[i+1])).setDistance(nodemap.get(namemap.get(sample[i+1])).getDistance() + finaldistance);
					finalpath.add((Node)nodemap.get(namemap.get(sample[i+1])).clone());
					finaldistance = finalpath.get(finalpath.size()-1).getDistance();
				}
				else
				{
					 nodemap.get(namemap.get(sample[i+1])).setTime(nodemap.get(namemap.get(sample[i+1])).getTime() + finaltime);
					 finalpath.add((Node)nodemap.get(namemap.get(sample[i+1])).clone());
					 finaltime = finalpath.get(finalpath.size()-1).getTime();
				}
				for(Map.Entry<Integer,Node> entry : nodemap.entrySet())
				{
					if(Integer.parseInt(sample[0]) % 2 == 0)
					{
						entry.getValue().setDistance(Integer.MAX_VALUE);
					}
					else
					{
						entry.getValue().setTime(Double.MAX_VALUE);
					}
					entry.getValue().setShortestPath(new LinkedList<Node>());
				}
		//		System.out.println(nodemap.get(namemap.get("Jammu")).getAdjacentNodes().get(nodemap.get(namemap.get("Chandigarh"))));
//				for(Node n : finalpath)
//				{
//					System.out.println(n.getName() +" "+n.getDistance());
//				}
			}
			for(int i=0;i<finalpath.size()-1;i++)
			{
				finalpath.get(i).addDestination(finalpath.get(i+1),finalpath.get(i).getAdjacentNodes().get(nodemap.get(namemap.get(finalpath.get(i+1).getName()))));
				finalpath.get(i).addDestinationTime(finalpath.get(i+1),finalpath.get(i).getAdjacentNodesTime().get(nodemap.get(namemap.get(finalpath.get(i+1).getName()))));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return finalpath;
	}
}